## msg
Is there a genertic-ish library that we can use to send messages?

Yes, protocolbuffer from the great guys at Google, it's touted as fast.  FIX8 Community Edition has something similar, and it's touted as fast too.

They both have the same problem. They do allocations per field. This is expensive, especially cleaning up in a multi-threaded environment, as there's logs of heap contention during message destruction.

## dependencies
Depends on
	wise_enum
	fmt
	spdlog
	boost
	azmq
	libzmq
	rapidxml

url = https://github.com/quicknir/wise_enum.git
url = https://github.com/fmtlib/fmt.git
url = https://github.com/gabime/spdlog.git
url = https://github.com/zeromq/azmq.git
url = https://github.com/zeromq/libzmq.git
url = https://github.com/timniederhausen/rapidxml.git

## running resource compiler
./rc -n ka::msg2 -a ../assets ../schema/simple.xml
