#pragma once

#include <fmt/format.h>
#include <wise_enum/wise_enum.h>

#include <cstddef>
#include <cstdlib>
#include <cstring>

#include <stdexcept>
#include <string>
#include <map>
#include <vector>
#include <utility>

namespace ka::msg2 {
// We need fixed length string types because they offer direct serialization.
// No allocations, pass by std::string_view, ... really fast
//
// NOTE: FixedString is NOT null terminated.
template <std::size_t LENGTH>
class FixedString {
public:
	static constexpr std::size_t SIZE = LENGTH;

	FixedString() = default;

	FixedString(const FixedString<SIZE>& n) :
		length_(n.length_) {
		std::strncpy(str_, n.str_, length_);
	}
	FixedString<SIZE>& operator=(const FixedString<SIZE>& n) {
		if (this == &n) return *this;
		length_ = n.length_;
		std::strncpy(str_, n.str_, length_);
		return *this;
	}

	FixedString(const char* value) {
		length_ = std::strlen(value);
		if (length_ > SIZE)
			error(length_);
		std::strncpy(str_, value, length_);
	}

	template <typename U> // string, string_view, vector<char> ...
	FixedString(const U& value) {
		if (value.size() > SIZE)
			error(value.size());
		std::strncpy(str_, value.data(), SIZE);
		length_ = value.size();
	}

	FixedString<SIZE>& cpy(std::string_view value) {
		if ((value.size() + size()) > SIZE)
			error(value.size());
		std::strncpy(str_, value.data(), value.size());
		length_ += value.size();
		return *this;
	}

	template <std::size_t N>
	FixedString<SIZE>& cpy(const FixedString<N>& value) {
		if ((value.size() + size()) > SIZE)
			error(value.size());
		std::strncpy(str_, value.data().data(), value.size());
		length_ += value.size();
		return *this;
	}

	FixedString<SIZE>& cat(std::string_view value) {
		if ((value.size() + size()) > SIZE)
			error(value.size());
		std::strncpy(str_ + length_, value.data(), value.size());
		length_ += value.size();
		return *this;
	}

	template <std::size_t N>
	FixedString<SIZE>& cat(const FixedString<N>& value) {
		if ((value.size() + size()) > SIZE)
			error(value.size());
		std::strncpy(str_ + length_, value.data().data(), value.size());
		length_ += value.size();
		return *this;
	}

	void clear() {
		length_ = 0;
	}

	bool empty() const {
		return length_ == 0;
	}

	std::size_t capacity() const {
		return SIZE;
	}

	std::size_t size() const {
		return length_;
	}

	std::string_view data() const {
		return {str_, length_};
	}

	const char* strchr(char token) const {
		for (int i = 0; i != length_; ++i)
			if (str_[i] == token)
				return str_ + i;
		return nullptr;
	}

	const char* strchr(const std::vector<char>& token) const {
		for (int i = 0; i != length_; ++i)
			for (std::size_t ti = 0; ti != token.size(); ++ti)
				if (str_[i] == token[ti])
					return str_ + i;
		return nullptr;
	}

	const char* strrchr(char token) const {
		for (int i = length_; i > 0; --i)
			if (str_[i] == token)
				return str_ + i;
		return nullptr;
	}

	const char* strrchr(const std::vector<char>& token) const {
		for (int i = length_; i > 0; --i)
			for (std::size_t ti = 0; ti != token.size(); ++ti)
				if (str_[i] == token[ti])
					return str_ + i;
		return nullptr;
	}

	template <std::size_t N>
	int compare(FixedString<N> value) const {
		for (std::size_t i = 0; i != size(); ++i) {
			if (i >= value.size())
				return 1;
			if (str_[i] < value[i])
				return -1;
			if (str_[i] > value[i])
				return 1;
		}
		return 0;
	}

	template <std::size_t N>
	bool equal(FixedString<N> value) const {
		if (value.size() > size())
			return false;
		for (std::size_t i = 0; i != SIZE; ++i)
			if (str_[i] != value[i])
				return false;
		return true;
	}

	char& operator[](std::size_t i) {
		return str_[i];
	}

	char operator[](std::size_t i) const {
		return str_[i];
	}

	char& at(std::size_t i) {
		if (i >= SIZE)
			throw std::out_of_range{fmt::format("index:{} larger than size:{}", i, size())};
		return str_[i];
	}

	char at(std::size_t i) const {
		if (i >= SIZE)
			throw std::out_of_range{fmt::format("index:{} larger than size:{}", i, size())};
		return str_[i];
	}

private:
	void error(std::size_t arg_length) const {
		throw std::runtime_error{fmt::format(
				"cannot add string of length {} to fixed string of size {} "
				"that already has a string of length {}",
				arg_length, SIZE, length_)};
	}

	char str_[SIZE];
	std::size_t length_{};
};

template <std::size_t N, std::size_t M>
inline bool operator<(const FixedString<N>& a, const FixedString<M>& b) {
	int rc = a.compare(b);
	return rc == -1;
}

template <std::size_t N, std::size_t M>
inline bool operator>(const FixedString<N>& a, const FixedString<M>& b) {
	int rc = a.compare(b);
	return rc == 1;
}

template <std::size_t N, std::size_t M>
inline bool operator==(const FixedString<N>& a, const FixedString<M>& b) {
	return a.equal(b);
}

std::string_view strchr(std::string_view in, char token) {
	for (std::size_t i = 0; i != in.size(); ++i)
		if (in.data()[i] == token)
			return {in.data(), i};
	return {};
}

using TinyString = FixedString<4>;
using SmallString = FixedString<16>;
using ShortString = FixedString<32>;
using MediumString = FixedString<64>;
using LongString = FixedString<128>;

using StdString = std::string;

// Field
WISE_ENUM_CLASS((TypeId, unsigned),
	Unassigned, Char, UnsignedChar, Int, UnsignedInt, Float, Double, FixedString, StdString
)

WISE_ENUM_CLASS((FieldId, unsigned),
	Unknown,
	Description,
	HeartbeatId,
	(End, FieldId::Description)
)

template <typename T, TypeId typeId, FieldId fieldId>
class Field {
public:
	using value_type = T;

	static constexpr TypeId tid() {
		return typeId;
	}

	static constexpr FieldId fid() {
		return fieldId;
	}

	const T& field() const {
		return value_;
	}

	void field(const T& value) {
		value_ = value;
	}

private:
	value_type value_;
};

using Description = Field<ShortString, TypeId::FixedString, FieldId::Description>;
using HeartbeatId = Field<unsigned, TypeId::UnsignedInt, FieldId::HeartbeatId>;

// Message
WISE_ENUM_CLASS((MsgId, unsigned),
	Unknown,
	Heartbeat,
	(End, Heartbeat)
)

namespace {
	inline char delimiter = '\x01';
}

class MsgBase {
public:
	MsgBase() = default;
	virtual ~MsgBase() = default;

	LongString toStream() const {
		return {};
	}

	static TinyString tag(MsgId id) {
		auto p = idmap_.find(id);
		if (p == idmap_.end())
			return {};
		return p->second;
	}

	static MsgId id(TinyString tag) {
		auto p = tagmap_.find(tag);
		if (p == tagmap_.end())
			return {};
		return p->second;
	}

	static std::unique_ptr<MsgBase> fromStream(std::string_view in);

protected:
	using IdMapTag = std::map<MsgId, TinyString>;
	using TagIdMap = std::map<TinyString, MsgId>;

	static inline const IdMapTag idmap_{
		{MsgId::Heartbeat, {"1\x01"}}
	};
	static inline const TagIdMap tagmap_{
		{{"1\x01"}, MsgId::Heartbeat}
	};
};

template <MsgId msgId>
class Msg : public MsgBase {
public:
	Msg() = default;
	static constexpr MsgId ID = MsgId::Unknown;
};

template <MsgId msgId>
inline LongString toStream(const Msg<msgId>& msg) {
	LongString stream;
	msg.toStream(stream);
	return stream;
}

template <>
class Msg<MsgId::Heartbeat> : public MsgBase {
public:
	Msg<MsgId::Heartbeat>() = default;
	Msg<MsgId::Heartbeat>(const Description::value_type& description) :
		description(description) {
	}

	static constexpr MsgId ID = MsgId::Heartbeat;
	static std::unique_ptr<MsgBase> fromStream(std::string_view in);

	template <typename U>
	void toStream(U& stream) const;

	Description::value_type description;
	HeartbeatId::value_type heartbeatId;
};

using HeartbeatMsg = Msg<MsgId::Heartbeat>;

inline std::unique_ptr<MsgBase> MsgBase::fromStream(std::string_view in) {
	TinyString tag = strchr(in, delimiter);
	MsgId msgId = id(tag);

	switch (msgId) {
	case MsgId::Heartbeat:
		return Msg<MsgId::Heartbeat>::fromStream({in.data() + tag.size() + 1, in.size() - tag.size() - 1});
	default:
		;
	}
	return {};
}

inline std::unique_ptr<MsgBase> Msg<MsgId::Heartbeat>::fromStream(std::string_view in) {
	Description::value_type description = strchr(in, delimiter);
	unsigned heartbeatId = static_cast<unsigned>( std::atoi(std::string(strchr(in, delimiter)).c_str()) );

//	std::unique_ptr<Msg<MsgId::Heartbeat>> msg{ new Msg<MsgId::Heartbeat>(description) };
	std::unique_ptr<Msg<MsgId::Heartbeat>> msg{ new Msg<MsgId::Heartbeat> };
	msg->description = description;
	msg->heartbeatId = heartbeatId;

	return std::unique_ptr<MsgBase>{ static_cast<MsgBase*>( msg.release() ) };
}

template <typename U>
inline void Msg<MsgId::Heartbeat>::toStream(U& stream) const {
	stream.cat("1\x01");

	stream.cat(description);
	stream.cat("\x01");

	stream.cat("7");
	stream.cat("\x01");
}
} // namespace kw::msg2
