#include "msg2.hpp"
//#include "../rc/message.hpp"

#include <rapidjson/rapidjson.h>
#include <rapidjson/document.h>

#include <spdlog/spdlog.h>
#include <fmt/format.h>

#include <azmq/actor.hpp>
#include <azmq/context.hpp>

#include <cstdlib>
#include <memory>
#include <stdexcept>
#include <string>

namespace ka {

namespace detail {
template <typename Document,
		  std::enable_if_t<std::is_class<Document>::value, bool> = true>
inline auto getObject(Document& doc, const char* name) {
	if (!doc.HasMember(name) || !doc[name].IsObject())
		throw std::runtime_error{fmt::format("{} as object missing", name)};
	return doc[name].GetObject();
}

template <typename Document,
		  std::enable_if_t<std::is_class<Document>::value, bool> = true>
inline auto getString(Document& doc, const char* name) {
	if (!doc.HasMember(name) || !doc[name].IsString())
		throw std::runtime_error{fmt::format("{} as string missing", name)};
	return doc[name].GetString();
}

template <typename Document,
		  std::enable_if_t<std::is_class<Document>::value, bool> = true>
inline auto getInt(Document& doc, const char* name) {
	if (!doc.HasMember(name) || !doc[name].IsInt())
		throw std::runtime_error{fmt::format("{} as int missing", name)};
	return doc[name].GetInt();
}
} // namespace detail

class IPubSub {
public:
	virtual ~IPubSub() = default;

	virtual void jsonCommand(std::string_view spec) {
	}

	virtual void subscribe(std::string_view host, std::uint16_t port, std::string_view symbol) {
	}

	virtual void unsubscribe(std::string_view symbol) {
	}

	template <typename UpdateType>
	void onUpdate(UpdateType& update) {
	}
};

class PubSub : public IPubSub {
public:
	void jsonCommand(std::string_view spec) override {
		using namespace detail;

		rapidjson::Document doc;
		doc.Parse(spec.data(), spec.size());
		if (!doc.IsObject())
			throw std::runtime_error{fmt::format("parse failed: {}", spec)};

		std::string action = getString(doc, "action");
		std::string symbol = getString(doc, "symbol");
		if (action == "unsubscribe") {
			unsubscribe(symbol);
			return;
		}

		auto connection = getObject(doc, "connection");
		std::string host = getString(connection, "host");
		std::string port = getString(connection, "port");
		if (action == "subscribe") {
			subscribe(host, std::atoi(port.c_str()), symbol);
			return;
		}

		throw std::runtime_error{fmt::format("unknown action: {}", action)};
	}

	void subscribe(std::string_view host, std::uint16_t port, std::string_view symbol) override {
		spdlog::info("subscribe: host={} port={} symbol={}", host, port, symbol);
	}

	void unsubscribe(std::string_view symbol) override {
		spdlog::info("unsubscribe: symbol={}", symbol);
	}

	template <typename UpdateType>
	void onUpdate(UpdateType& update) {
	}

private:
};

std::unique_ptr<IPubSub> factory() {
	return std::unique_ptr<IPubSub>{ new PubSub };
}
} // namespace ka

namespace serialization {
constexpr std::string_view subscribe_request{
	R"({"action": "subscribe")"
	R"(, "connection": {"host": "localhost", "port": "2023"})"
	R"(, "symbol": "BTC-ETH")"
	R"(})"
};
constexpr std::string_view unsubscribe_request{
	R"({"action": "unsubscribe")"
	R"(, "symbol": "BTC-ETH")"
	R"(})"
};

void run() {
	boost::asio::io_service ios;

	std::unique_ptr<ka::IPubSub> ps{ ka::factory() };
	ps->jsonCommand(subscribe_request);
	ps->jsonCommand(unsubscribe_request);
}
} // serialization

namespace basic_serialization {
void run() {
	using namespace ka::msg2;

/*
	spdlog::info("sizeof Field<char, TypeId::Char>: {}", sizeof(Field<char, TypeId::Char, FieldId::Description>));
	spdlog::info("sizeof Field<unsigned char, TypeId::UnsignedChar>: {}", sizeof(Field<unsigned char, TypeId::UnsignedChar, FieldId::Description>));
	spdlog::info("sizeof Field<int, TypeId::Int>: {}", sizeof(Field<int, TypeId::Int, FieldId::Description>));
	spdlog::info("sizeof Field<unsigned int, TypeId::UnsignedInt>: {}", sizeof(Field<unsigned int, TypeId::UnsignedInt, FieldId::Description>));
	spdlog::info("sizeof Field<float, TypeId::Float>: {}", sizeof(Field<float, TypeId::Float, FieldId::Description>));
	spdlog::info("sizeof Field<double, TypeId::Double>: {}", sizeof(Field<double, TypeId::Double, FieldId::Description>));
	spdlog::info("sizeof Field<char[64], TypeId::FixedString>: {}", sizeof(Field<char[64], TypeId::FixedString, FieldId::Description>));
 */

	LongString stream;
	{
		HeartbeatMsg("have a heart").toStream(stream);
	}
	{
		auto msg = MsgBase::fromStream(stream.data());
		if (msg.get()) {
			HeartbeatMsg* heartbeat = static_cast<HeartbeatMsg*>(msg.get());
			spdlog::info("hearbeaat: {}", heartbeat->description.data());
		}
	}

	stream = "1hacked message";
	auto msg = MsgBase::fromStream(stream.data());
	if (msg.get()) {
		HeartbeatMsg* heartbeat = static_cast<HeartbeatMsg*>(msg.get());
		spdlog::info("hearbeaat: {}", heartbeat->description.data());
	}
}
} // basic_serialization

namespace router_dealer {
void run() {
	// create router
	boost::asio::io_service ios_router;
	azmq::socket router(ios_router, ZMQ_ROUTER);
	router.bind(std::string("inproc://") + BOOST_CURRENT_FUNCTION);

	// create dealer
	boost::asio::io_service ios_dealer;
	azmq::socket dealer(ios_dealer, ZMQ_DEALER);
	dealer.connect(std::string("inproc://") + BOOST_CURRENT_FUNCTION);

	// setup send callback
	using namespace ka::msg2;
	HeartbeatMsg heartbeat("have a heart");
	spdlog::info("dealer hearbeaat: {}", heartbeat.description.data());
	LongString stream = toStream(heartbeat);
	std::uint32_t stream_length = htonl(stream.size());
	std::array<boost::asio::const_buffer, 2> snd_bufs = {{
		boost::asio::buffer(stream.data(), stream.size()),
		boost::asio::buffer((void*)&stream_length, sizeof(std::uint32_t))
	}};

	boost::system::error_code dealer_ec;
	size_t dealer_bytes_sent = 0;
	dealer.async_send(snd_bufs, [&] (boost::system::error_code const& ec, size_t nbytes) {
		SCOPE_EXIT { ios_dealer.stop(); };
		dealer_ec = ec;
		dealer_bytes_sent = nbytes;
		spdlog::info("delaer.async_send: nbytes:{}, snd_bufs[0]<{}>{} snd_bufs[1]<>{}",
			nbytes,
			std::string_view{(char*)snd_bufs[0].data(), snd_bufs[0].size()}, snd_bufs[0].size(),
			snd_bufs[1].size());
	});

	// setup receive callback
	std::array<char, 5> ident;
	std::array<char, ka::msg2::LongString::SIZE> recv_stream;
	std::array<char, sizeof(std::uint32_t)> recv_stream_length;
	std::array<boost::asio::mutable_buffer, 3> recv_bufs = {{
		boost::asio::buffer(ident), // dealer id
		boost::asio::buffer(recv_stream),
		boost::asio::buffer(recv_stream_length)
	}};

	boost::system::error_code router_ec;
	size_t router_bytes_received = 0;
	router.async_receive(recv_bufs, [&](boost::system::error_code const& ec, size_t nbytes) {
		SCOPE_EXIT { ios_router.stop(); };
		router_ec = ec;
		router_bytes_received = nbytes;
		spdlog::info("router.async_receive: nbytes:{}, recv_bufs[1]:{} recv_bufs[2]:{}",
			nbytes, recv_bufs[1].size(), recv_bufs[2].size());

		std::uint32_t recv_stream_length = ntohl(*(std::uint32_t*)recv_bufs[2].data());
		std::string_view recv_stream{(char*)recv_bufs[1].data(), (std::size_t)recv_stream_length};

		auto msg = MsgBase::fromStream(recv_stream);
		if (msg.get()) {
			HeartbeatMsg* heartbeat = static_cast<HeartbeatMsg*>(msg.get());
			spdlog::info("router received hearbeaat: {}", heartbeat->description.data());
		}
	});

	// do it
	ios_dealer.run();
	ios_router.run();
}
} //router_dealer

namespace push_pull {
void run() {
	// create router
	boost::asio::io_service ios_push;
	azmq::socket push(ios_push, ZMQ_PUSH);
	push.bind(std::string("inproc://") + BOOST_CURRENT_FUNCTION);

	// create dealer
	boost::asio::io_service ios_pull;
	azmq::socket pull(ios_pull, ZMQ_PULL);
	pull.connect(std::string("inproc://") + BOOST_CURRENT_FUNCTION);

	// setup send callback
	using namespace ka::msg2;
	HeartbeatMsg heartbeat("have a heart");
	spdlog::info("push hearbeaat: {}", heartbeat.description.data());
	LongString stream = toStream(heartbeat);
	std::uint32_t stream_length = htonl(stream.size());
	std::array<boost::asio::const_buffer, 2> snd_bufs = {{
		boost::asio::buffer(stream.data(), stream.size()),
		boost::asio::buffer((void*)&stream_length, sizeof(std::uint32_t))
	}};

	boost::system::error_code push_ec;
	size_t push_bytes_sent = 0;
	push.async_send(snd_bufs, [&] (boost::system::error_code const& ec, size_t nbytes) {
		SCOPE_EXIT { ios_push.stop(); };
		push_ec = ec;
		push_bytes_sent = nbytes;
		spdlog::info("push.async_send: nbytes:{}, snd_bufs[0]<{}>{} snd_bufs[1]<>{}",
			nbytes,
			std::string_view{(char*)snd_bufs[0].data(), snd_bufs[0].size()}, snd_bufs[0].size(),
			snd_bufs[1].size());
	});

	// setup receive callback
	std::array<char, ka::msg2::LongString::SIZE> recv_stream;
	std::array<char, sizeof(std::uint32_t)> recv_stream_length;
	std::array<boost::asio::mutable_buffer, 2> recv_bufs = {{
		boost::asio::buffer(recv_stream),
		boost::asio::buffer(recv_stream_length)
	}};

	boost::system::error_code pull_ec;
	size_t pull_bytes_received = 0;
	pull.async_receive(recv_bufs, [&](boost::system::error_code const& ec, size_t nbytes) {
		SCOPE_EXIT { ios_pull.stop(); };
		pull_ec = ec;
		pull_bytes_received = nbytes;
		spdlog::info("pull.async_receive: nbytes:{}", nbytes);

		std::uint32_t recv_stream_length = ntohl(*(std::uint32_t*)recv_bufs[1].data());
		std::string_view recv_stream{(char*)recv_bufs[0].data(), (std::size_t)recv_stream_length};

		auto msg = MsgBase::fromStream(recv_stream);
		if (msg.get()) {
			HeartbeatMsg* heartbeat = static_cast<HeartbeatMsg*>(msg.get());
			spdlog::info("pull received hearbeaat: {}", heartbeat->description.data());
		}
	});

	// do it
	ios_push.run();
	ios_pull.run();
}
} // push_pull

namespace pair_pair {
void run() {
	// create router
	boost::asio::io_service ios_push;
	azmq::socket push(ios_push, ZMQ_PAIR);
	push.bind(std::string("inproc://") + BOOST_CURRENT_FUNCTION);

	// create dealer
	boost::asio::io_service ios_pull;
	azmq::socket pull(ios_pull, ZMQ_PAIR);
	pull.connect(std::string("inproc://") + BOOST_CURRENT_FUNCTION);

	// setup send callback
	using namespace ka::msg2;
	HeartbeatMsg heartbeat("have a heart");
	spdlog::info("pair-push hearbeaat: {}", heartbeat.description.data());
	LongString stream = toStream(heartbeat);
	std::uint32_t stream_length = htonl(stream.size());
	std::array<boost::asio::const_buffer, 2> snd_bufs = {{
		boost::asio::buffer(stream.data(), stream.size()),
		boost::asio::buffer((void*)&stream_length, sizeof(std::uint32_t))
	}};

	boost::system::error_code push_ec;
	size_t push_bytes_sent = 0;
	push.async_send(snd_bufs, [&] (boost::system::error_code const& ec, size_t nbytes) {
		SCOPE_EXIT { ios_push.stop(); };
		push_ec = ec;
		push_bytes_sent = nbytes;
		spdlog::info("pair-push.async_send: nbytes:{}, snd_bufs[0]<{}>{} snd_bufs[1]<>{}",
			nbytes,
			std::string_view{(char*)snd_bufs[0].data(), snd_bufs[0].size()}, snd_bufs[0].size(),
			snd_bufs[1].size());
	});

	// setup receive callback
	std::array<char, ka::msg2::LongString::SIZE> recv_stream;
	std::array<char, sizeof(std::uint32_t)> recv_stream_length;
	std::array<boost::asio::mutable_buffer, 2> recv_bufs = {{
		boost::asio::buffer(recv_stream),
		boost::asio::buffer(recv_stream_length)
	}};

	boost::system::error_code pull_ec;
	size_t pull_bytes_received = 0;
	pull.async_receive(recv_bufs, [&](boost::system::error_code const& ec, size_t nbytes) {
		SCOPE_EXIT { ios_pull.stop(); };
		pull_ec = ec;
		pull_bytes_received = nbytes;
		spdlog::info("pair-pull.async_receive: nbytes:{}", nbytes);

		std::uint32_t recv_stream_length = ntohl(*(std::uint32_t*)recv_bufs[1].data());
		std::string_view recv_stream{(char*)recv_bufs[0].data(), (std::size_t)recv_stream_length};

		auto msg = MsgBase::fromStream(recv_stream);
		if (msg.get()) {
			HeartbeatMsg* heartbeat = static_cast<HeartbeatMsg*>(msg.get());
			spdlog::info("pair-pull received hearbeaat: {}", heartbeat->description.data());
		}
	});

	// do it
	ios_push.run();
	ios_pull.run();
}
} // pair_pair

int main()
try {
	// https://spdlog.docsforge.com/v1.x/3.custom-formatting/#pattern-flags
	spdlog::set_pattern("%Y-%m-%d %T:%S.%f %^%L%$ %v");

	serialization::run();
	basic_serialization::run();
	router_dealer::run();
	push_pull::run();
	pair_pair::run();
}
catch (const std::exception& e) {
	spdlog::error("fatal: {}", e.what());
}
