#pragma once

#include <fmt/format.h>
#include <wise_enum/wise_enum.h>

#include <cstddef>
#include <stdexcept>
#include <string>
#include <vector>
#include <utility>

namespace ka::msg {
WISE_ENUM_CLASS((FieldId, unsigned),
	Unassigned, Char, UnsignedChar, Int, UnsignedInt, Float, Double,
	TinyString, SmallString, ShortString, MediumString, LongString, StdString
)

WISE_ENUM_CLASS((MsgId, unsigned),
	Heartbeat
)

class Field : public std::pair<FieldId, std::string_view> {
public:
	using inherited = std::pair<FieldId, std::string_view>;

	Field() : inherited(FieldId::Unassigned, std::string_view{}) {
	}
	Field(FieldId id, std::string_view content) : inherited(id, content) {
	}
};

class Stream : public std::pair<MsgId, std::vector<Field>> {
public:
	using inherited = std::pair<MsgId, std::vector<Field>>;
	using inherited::inherited;

	Stream() = default;
};

class MsgBase {
public:
	MsgBase() = default;
};

class Msg : public MsgBase {
public:
	virtual ~Msg() = default;
	virtual Stream toStream() const = 0;

	static Msg* fromStream(const Stream& in);
};

class HeartbeatMsg : public Msg {
public:
	HeartbeatMsg() = default;
	HeartbeatMsg(std::string_view payload) : payload_(payload) {
	}

	std::string_view payload() const {
		return payload_;
	}

	Stream toStream() const override {
		std::vector<Field> fields;
		fields.emplace_back(FieldId::StdString, std::string_view{payload_});

		return Stream(id_, std::move(fields));
	}

private:
	static inline constexpr auto id_ = MsgId::Heartbeat;
	std::string payload_;
};

inline Msg* Msg::fromStream(const Stream &in) {
	if (in.second.empty())
		throw std::runtime_error{"empty Stream in Msg::fromStream"};

	switch (in.first) {
	case MsgId::Heartbeat:
		if (in.second.size() < 1)
			throw std::runtime_error{"Heartbeat expects 1 parameter in Msg::fromStream"};
		return new HeartbeatMsg(in.second[0].second);
	default:
		throw std::runtime_error{fmt::format("unhandled MsgId={} in Msg::fromStream", wise_enum::to_string(in.first))};
	}
}
} // namespace ka::msg
