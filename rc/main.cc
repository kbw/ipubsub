#include <rapidxml/rapidxml.hpp>

#include <spdlog/spdlog.h>

#include <cstring>
#include <deque>
#include <fstream>
#include <iterator>
#include <list>
#include <map>
#include <optional>
#include <string>
#include <vector>

class Type {
public:
	using TypesType = std::list<Type>;
	using TypesIter = TypesType::iterator;

	Type() = delete;
	Type(std::string name, std::string definition) :
		name_(std::move(name)), definition_(std::move(definition)) {
		types.push_front(*this);
		auto rc_name = by_name.insert({name_, types.begin()});
		if (!rc_name.second)
			throw std::runtime_error{"duplicate type name:" + name_};
	}

	std::string name() const {
		return name_;
	}

	std::string definition() const {
		return definition_;
	}

	std::string baseDefinition() const {
		auto pos = definition_.find('<');
		if (pos == std::string::npos)
			return definition_;
		return definition_.substr(0, pos);
	}

	static const TypesType& all() {
		return types;
	}

	static bool haveName(const std::string& name) {
		auto p = by_name.find(name);
		return p != by_name.end();
	}

	static const Type& byName(const std::string& name) {
		auto p = by_name.find(name);
		if (p == by_name.end())
			throw std::runtime_error{"missing type name:" + name};
		return *p->second;
	}

private:
	std::string name_;
	std::string definition_;

	inline static std::list<Type> types;
	inline static std::map<std::string, TypesIter> by_name;
};

class Field {
public:
	using FieldsType = std::list<Field>;
	using FieldsIter = FieldsType::iterator;

	Field() = delete;
	Field(unsigned id, std::string name, std::string type) :
		id_(id), name_(std::move(name)), type_(std::move(type)) {
		fields.push_front(*this);
		auto rc_id = by_id.insert({id_, fields.begin()});
		if (!rc_id.second)
			throw std::runtime_error{"duplicate field id:" + std::to_string(id_)};
		auto rc_name = by_name.insert({name_, fields.begin()});
		if (!rc_name.second)
			throw std::runtime_error{"duplicate field name:" + name_};
	}

	unsigned id() const {
		return id_;
	}

	std::string name() const {
		return name_;
	}

	std::string type() const {
		return type_;
	}

	static FieldsType& all() {
		return fields;
	}

	static bool haveId(unsigned id) {
		auto p = by_id.find(id);
		return p != by_id.end();
	}

	static Field& byId(unsigned id) {
		auto p = by_id.find(id);
		if (p == by_id.end())
			throw std::runtime_error{"missing field id:" + std::to_string(id)};
		return *p->second;
	}

	static bool haveName(const std::string& name) {
		auto p = by_name.find(name);
		return p != by_name.end();
	}

	static Field& byName(const std::string& name) {
		auto p = by_name.find(name);
		if (p == by_name.end())
			throw std::runtime_error{"missing field name:" + name};
		return *p->second;
	}

private:
	unsigned id_;
	std::string name_;
	std::string type_;

	inline static std::list<Field> fields;
	inline static std::map<unsigned, FieldsIter> by_id;
	inline static std::map<std::string, FieldsIter> by_name;
};

class Message {
public:
	using MessagesType = std::list<Message>;
	using MessagesIter = MessagesType::iterator;

	Message() = delete;
	Message(unsigned id, std::string name) :
		id_(id), name_(std::move(name)) {
		messages.push_front(*this);
		auto rc_id = by_id.insert({id_, messages.begin()});
		if (!rc_id.second)
			throw std::runtime_error{"duplicate message id:" + std::to_string(id_)};
		auto rc_name = by_name.insert({name_, messages.begin()});
		if (!rc_name.second)
			throw std::runtime_error{"duplicate message name:" + name_};
	}

	void addField(unsigned fieldId, bool required) {
		byId(id_).fields_.push_back({fieldId, required});
	}

	unsigned id() const {
		return id_;
	}

	std::string name() const {
		return name_;
	}

	std::vector<unsigned> fields() const {
		std::vector<unsigned> selected;
		for (auto field : fields_)
			if (field.second)
				selected.push_back(field.first);
		return selected;
	}

	static const MessagesType& all() {
		return messages;
	}

	static bool haveId(unsigned id) {
		auto p = by_id.find(id);
		return p != by_id.end();
	}

	static Message& byId(unsigned id) {
		auto p = by_id.find(id);
		if (p == by_id.end())
			throw std::runtime_error{"missing message id:" + std::to_string(id)};
		return *p->second;
	}

	static bool haveName(const std::string& name) {
		auto p = by_name.find(name);
		return p != by_name.end();
	}

	static Message& byName(const std::string& name) {
		auto p = by_name.find(name);
		if (p == by_name.end())
			throw std::runtime_error{"missing message name:" + name};
		return *p->second;
	}

private:
	unsigned id_;
	std::string name_;
	std::vector<std::pair<unsigned, bool>> fields_; // {fieldId, required}

	inline static std::list<Message> messages;
	inline static std::map<unsigned, MessagesIter> by_id;
	inline static std::map<std::string, MessagesIter> by_name;
};

struct Options {
	Options(int argc, char* argv[]);

	std::optional<std::string> ns; // namespace
	std::optional<std::string> asset_dir; // assets directory
	std::string input_schema; // input xml
	std::string output_file{"message.hpp"};
};

void parseInputs(const Options& opts);
void copy(std::istream& is, std::ostream& os);
void copy(const Options& opts, std::string filename, std::ostream& os);
void copy_preamble(const Options& opts, std::string filename, std::ostream& os);
void copy_postamble(const Options& opts, std::string filename, std::ostream& os);
void copy_types(const Options& opts, std::ostream& os);
void copy_fields(const Options& opts, std::ostream& os);
void copy_field_enums(const Options& opts, std::ostream& os);
void copy_message_enums(const Options& opts, std::ostream& os);
void copy_message(const Options& opts, std::ostream& os);
std::string tolower(const std::string& str);

int main(int argc, char* argv[]) {
	Options opts(argc, argv);

	if (opts.input_schema.empty())
		spdlog::info("no schema");

	if (opts.ns)
		spdlog::info("namespace: {}", *opts.ns);

	if (opts.asset_dir)
		spdlog::info("namespace: {}", *opts.asset_dir);

	parseInputs(opts);

	std::ofstream os(opts.output_file, std::ios::trunc);
	copy_preamble(opts, "FilePreamble.txt", os);
	copy(opts, "FixedString.txt", os);
	copy_types(opts, os);
//	copy(opts, "String.txt", os);
	copy(opts, "FieldBegin.txt", os);
	copy_field_enums(opts, os);
	copy(opts, "FieldEnd.txt", os);
	copy_fields(opts, os);
	copy(opts, "MessageBegin.txt", os);
	copy_message_enums(opts, os);
	copy(opts, "MessageEnd.txt", os);
	copy(opts, "Message.txt", os);
	copy_message(opts, os);
	copy_postamble(opts, "FilePostamble.txt", os);
}

Options::Options(int argc, char* argv[]) {
	enum expected_values { expect_namespace, expect_asset_dir, expect_output_file };
	std::deque<expected_values> expectations;

	for (int i = 1; i < argc; ++i) {
		const char* arg = argv[i];

		if (std::strcmp(arg, "-n") == 0 || std::strcmp(arg, "--namespace") == 0) {
			expectations.push_back(expect_namespace);
			continue;
		} else if (std::strcmp(arg, "-a") == 0 || std::strcmp(arg, "--asset-dir") == 0) {
			expectations.push_back(expect_asset_dir);
			continue;
		} else if (std::strcmp(arg, "-o") == 0 || std::strcmp(arg, "--output-file") == 0) {
			expectations.push_back(expect_output_file);
			continue;
		}

		if (expectations.empty()) {
			if (input_schema.size())
				throw std::runtime_error{"input file=" + input_schema + " already specified"};
			input_schema = arg;
			continue;
		}

		switch (expectations.front()) {
		case expect_namespace:
			ns = arg;
			break;
		case expect_asset_dir:
			asset_dir = arg;
			break;
		case expect_output_file:
			output_file = arg;
			break;
		}
		expectations.pop_front();
	};
}

void parseInputs(const Options& opts) {
	if (opts.input_schema.size()) {
		std::ifstream is(opts.input_schema);
		std::string xmlstr{std::istreambuf_iterator<char>(is), std::istreambuf_iterator<char>()};
		xmlstr += '\0';
		spdlog::info("length:{}\n{}\n{}", xmlstr.size(), opts.input_schema, xmlstr);

		rapidxml::xml_document<> doc;
		doc.parse<0>(xmlstr.data());

		rapidxml::xml_node<>* root_node = doc.first_node("ka");
		spdlog::info("{}: major:{} minor:{} revision:{}",
			"ka",
			root_node->first_attribute("major")->value(),
			root_node->first_attribute("minor")->value(),
			root_node->first_attribute("revision")->value());

		// types
		for (rapidxml::xml_node<>* types_node = root_node->first_node("types"); types_node; types_node = types_node->next_sibling()) {
			for (rapidxml::xml_node<>* type_node = types_node->first_node("type"); type_node; type_node = type_node->next_sibling()) {
				if (!type_node->first_attribute("name") ||
					!type_node->first_attribute("definition"))
					throw std::runtime_error{"missing field in Type"};
				spdlog::info(" {}: name:{} definition:{}",
					"type",
					type_node->first_attribute("name")->value(),
					type_node->first_attribute("definition")->value());
				Type{
					type_node->first_attribute("name")->value(),
					type_node->first_attribute("definition")->value()};
			}
		}

		// fields
		for (rapidxml::xml_node<>* fields_node = root_node->first_node("fields"); fields_node; fields_node = fields_node->next_sibling()) {
			for (rapidxml::xml_node<>* field_node = fields_node->first_node("field"); field_node; field_node = field_node->next_sibling()) {
				if (!field_node->first_attribute("number") ||
					!field_node->first_attribute("name") ||
					!field_node->first_attribute("type"))
					throw std::runtime_error{"missing field in Field"};
				spdlog::info(" {}: number:{} name:{} type:{}",
					"field",
					field_node->first_attribute("number")->value(),
					field_node->first_attribute("name")->value(),
					field_node->first_attribute("type")->value());
				Field{
					(unsigned)std::atoi(field_node->first_attribute("number")->value()),
					field_node->first_attribute("name")->value(),
					field_node->first_attribute("type")->value()};
			}
		}

		// messages
		for (rapidxml::xml_node<>* messages_node = root_node->first_node("messages"); messages_node; messages_node = messages_node->next_sibling()) {
			for (rapidxml::xml_node<>* message_node = messages_node->first_node("message"); message_node; message_node = message_node->next_sibling()) {
				if (!message_node->first_attribute("name") ||
					!message_node->first_attribute("msgtype"))
					throw std::runtime_error{"missing field in Message"};
				spdlog::info(" {}: name:{} msgtype:{}",
					"message",
					message_node->first_attribute("name")->value(),
					message_node->first_attribute("msgtype")->value());
				Message message{
					(unsigned)std::atoi(message_node->first_attribute("msgtype")->value()),
					message_node->first_attribute("name")->value()};
				for (rapidxml::xml_node<>* field_node = message_node->first_node("field"); field_node; field_node = field_node->next_sibling()) {
					if (!field_node->first_attribute("name") ||
						!field_node->first_attribute("required"))
						throw std::runtime_error{"missing field in Field in Message:" + std::string(message_node->first_attribute("name")->value())};
					spdlog::info(" {}: name:{} required:{}",
						" Message::field",
						field_node->first_attribute("name")->value(),
						field_node->first_attribute("required")->value());

					// lookup fieldid
					auto& field = Field::byName(field_node->first_attribute("name")->value());
					spdlog::info(" {}(FieldId:{}, required:{})",
						" message.addField",
						field.id(),
						(std::strcmp("Y", field_node->first_attribute("required")->value()) == 0 ? true : false));
					message.addField(field.id(), std::strcmp("Y", field_node->first_attribute("required")->value()) == 0);
				}
			}
		}
	}
}

void copy(std::istream& is, std::ostream& os) {
	char buffer[4096];
	do {
		is.read(buffer, sizeof(buffer));
		os.write(buffer, is.gcount());
	} while (is.gcount());
}

void copy(const Options& opts, std::string filename, std::ostream& os) {
	std::string pathname;
	if (opts.asset_dir)
		pathname += *opts.asset_dir;
	if (pathname.size() && pathname.back() != '/')
		pathname += '/';
	pathname += filename;

	std::ifstream is(pathname);
	copy(is, os);
}

void copy_preamble(const Options& opts, std::string filename, std::ostream& os) {
	copy(opts, filename, os);
	if (opts.ns)
		os << "namespace " << *opts.ns << " {\n\n";
}

void copy_postamble(const Options& opts, std::string filename, std::ostream& os) {
	copy(opts, filename, os);
	if (opts.ns)
		os << "} // namesapce " << *opts.ns << "\n";
}

void copy_types(const Options& opts, std::ostream& os) {
	for (auto p = Type::all().crbegin(); p != Type::all().crend(); ++p)
		os << "using " << p->name() << " = " << p->definition() << ";\n";
}

void copy_fields(const Options& opts, std::ostream& os) {
	for (auto field_type = Field::all().crbegin(); field_type != Field::all().crend(); ++field_type) {
		if (Type::haveName(field_type->type())) {
			os << "using " << field_type->name() << " = Field<" << field_type->type() << ", TypeId::" << Type::byName(field_type->type()).baseDefinition() << ", FieldId::" << field_type->name() << ">;\n";
		}
	}
}

void copy_field_enums(const Options& opts, std::ostream& os) {
	for (auto p = Field::all().crbegin(); p != Field::all().crend(); ++p)
		os << "\t" << p->name() << "\n";
}

void copy_message_enums(const Options& opts, std::ostream& os) {
	for (auto p = Message::all().crbegin(); p != Message::all().crend(); ++p)
		os << "\t" << p->name() << ",\n";
}

void copy_message(const Options& opts, std::ostream& os) {
	// class Msg<MsgId::Message>
	for (auto p = Message::all().crbegin(); p != Message::all().crend(); ++p) {
		const auto fields = p->fields();

		os<<"template <>\n";
		os<<"class Msg<MsgId::" << p->name() << "> : public MsgBase {\n";
		os<<"public:\n";
		os<<"	Msg<MsgId::" << p->name() << ">() = default;\n";
		if (fields.size()) {
			// Field parameters to constructor
			os<<"	Msg<MsgId::" << p->name() << ">(\n";
			for (std::size_t i = 1; i < fields.size(); ++i) {
				const Field& field = Field::byId(fields[i - 1]);
				os<<"			const " << field.name() << "::value_type& " << tolower(field.name()) << ",\n";
			}
			os<<"			const " << Field::byId(fields.back()).name() << "::value_type& " << tolower(Field::byId(fields.back()).name()) << ") :\n";

			// Field initialisation
			for (std::size_t i = 1; i < fields.size(); ++i) {
				const Field& field = Field::byId(fields[i - 1]);
				os<<"		" << tolower(field.name()) << "(" << tolower(field.name()) << "),\n";
			}
			os<<"		" << tolower(Field::byId(fields.back()).name()) << "(" << tolower(Field::byId(fields.back()).name()) << ") {\n";
			os<<"	}\n";
		}
		os<<"\n";
		os<<"	static constexpr MsgId ID = MsgId::" << p->name() << ";\n";
		os<<"	static std::unique_ptr<MsgBase> fromStream(std::string_view in);\n";
		os<<"\n";
		os<<"	template <typename U>\n";
		os<<"	void toStream(U& stream) const;\n";
		os<<"\n";
		if (fields.size()) {
			for (std::size_t i = 0; i < fields.size(); ++i) {
				const Field& field = Field::byId(fields[i]);
				os<<"	" << field.name() << "::value_type " << tolower(field.name()) << ";\n";
			}
		}
		os<<"};\n";
		os<<"\n";
	}

	// forward declare messages
	for (auto p = Message::all().crbegin(); p != Message::all().crend(); ++p) {
		os<<"using " << p->name() << "Msg = Msg<MsgId::" << p->name() << ">;\n";
		os<<"\n";
	}

	// MsgBase::fromStream
	os<<"inline std::unique_ptr<MsgBase> MsgBase::fromStream(std::string_view in) {\n";
	os<<"	TinyString tag = strchr(in, delimiter);\n";
	os<<"	MsgId msgId = id(tag);\n";
	os<<"\n";
	os<<"	switch (msgId) {\n";
	for (auto p = Message::all().crbegin(); p != Message::all().crend(); ++p) {
		os<<"	case MsgId::" << p->name() << ":\n";
		os<<"		return Msg<MsgId::" << p->name() << ">::fromStream({in.data() + tag.size() + 1, in.size() - tag.size() - 1});\n";
	}
	os<<"	default:\n";
	os<<"		;\n";
	os<<"	}\n";
	os<<"	return {};\n";
	os<<"}\n";
	os<<"\n";

	// Message::fromStream
	for (auto p = Message::all().crbegin(); p != Message::all().crend(); ++p) {
		const auto fields = p->fields();

		os<<"inline std::unique_ptr<MsgBase> Msg<MsgId::" << p->name() << ">::fromStream(std::string_view in) {\n";
		os<<"	Description::value_type description = strchr(in, delimiter);\n";
		os<<"\n";
		os<<"	std::unique_ptr<Msg<MsgId::" << p->name() << ">> msg{ new Msg<MsgId::" << p->name() << "> };\n";

		// Field assignment
		for (std::size_t i = 0; i < fields.size(); ++i) {
			const Field& field = Field::byId(fields[i]);
			os<<"	msg->" << tolower(field.name()) << " = " << tolower(field.name()) << ";\n";
		}

		os<<"\n";
		os<<"	return std::unique_ptr<MsgBase>{ static_cast<MsgBase*>( msg.release() ) };\n";
		os<<"}\n";
		os<<"\n";
		os<<"template <typename U>\n";
		os<<"inline void Msg<MsgId::" << p->name() << ">::toStream(U& stream) const {\n";
		os<<"	stream.cat(\"" << p->id() << "\\x01\");\n";
		for (std::size_t i = 0; i < fields.size(); ++i) {
			const Field& field = Field::byId(fields[i]);
			os<<"\n";
			os<<"	stream.cat(" << tolower(field.name()) << ");\n";
			os<<"	stream.cat(\"\\x01\");\n";
		}
		os<<"}\n";
		os<<"\n";
	}
}

std::string tolower(const std::string& str) {
	std::string out = str;
	if (out.size())
		out[0] = std::tolower(out[0]);
	return out;
}
