//	End
)

template <typename T, TypeId typeId, FieldId fieldId>
class Field {
public:
	using value_type = T;

	static constexpr TypeId tid() {
		return typeId;
	}

	static constexpr FieldId fid() {
		return fieldId;
	}

	const T& field() const {
		return value_;
	}

	void field(const T& value) {
		value_ = value;
	}

private:
	value_type value_;
};

